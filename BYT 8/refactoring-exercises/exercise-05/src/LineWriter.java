
public class LineWriter {
	private FieldWriter fw = new FieldWriter();
	
	public void writeLine(String[] fields) {
		if (fields.length == 0)
			System.out.println();
		else {
			fw.writeField(fields[0]);

			for (int i = 1; i < fields.length; i++) {
				System.out.print(",");
				fw.writeField(fields[i]);
			}
			System.out.println();
		}
	}
}
