

public class FieldWriter {
	private QuoteWriter qw = new QuoteWriter();

	public void writeField(String field) {
		if (field.indexOf(',') != -1 || field.indexOf('\"') != -1)
			qw.writeQuoted(field);
		else
			System.out.print(field);
	}

}
