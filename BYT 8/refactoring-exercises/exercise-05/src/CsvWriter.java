public class CsvWriter {
	public CsvWriter() {
	}

	/*
	 * BS : Divergent Change
	 * REF : Extract class
	 */
	private LineWriter lw = new LineWriter();

	public void write(String[][] lines) {
		for (int i = 0; i < lines.length; i++)
			lw.writeLine(lines[i]);
	}
}